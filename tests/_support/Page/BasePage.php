<?php


namespace Page;


use AcceptanceTester;

class BasePage
{

    public $config;
    /**
     * @var AcceptanceTester
     */
    protected $tester;


    /**
     * BasePage constructor.
     *
     * @param AcceptanceTester $I
     * @param $config
     */
    public function __construct(\AcceptanceTester $I, $config = null){
        $this->config = $config;
        $this->tester = $I;
    }

}